-module(word_count).
-export([count_me/1]).

count_me(Str) -> count_items(string:tokens(Str, "  ")).

count_items([]) -> 0;
count_items([_ | Tail]) -> 1 + count_items(Tail).

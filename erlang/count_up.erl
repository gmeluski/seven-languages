-module(count_up).
-export([count_up/0]).

count_up() -> counting(0).

counting(10) -> 10;
counting(N) -> 
  io:fwrite("~w~n", [N]), 
  counting(N + 1).

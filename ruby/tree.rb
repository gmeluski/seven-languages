=begin
class Tree
  attr_accessor :children, :node_name

  def initialize(name, children=[])
    @children = children
    @node_name = name
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end 

  def visit(&block)
    block.call self
  end
end

ruby_tree = Tree.new("Ruby", [Tree.new("Reia"), Tree.new("MacRuby")])

puts "Visiting a node"
ruby_tree.visit {|node| puts node.node_name}
puts

=end

class Tree
  attr_accessor :children, :node_name

  def initialize(tree={})
    if tree.keys.length > 1
      raise "Tree has more than one root"
    end
    @children = [] 

    @node_name = tree.keys[0]
    descendents = tree[@node_name]
    puts "descendents: #{descendents}"

    if descendents.empty?
      puts "No descendents, done"
      return
    end

    descendents.each do |key, value|
      puts "key: #{key}"
      child = Tree.new({key => value})
      children.append(child)
    end
  end

  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end 

  def visit(&block)
    block.call self
  end
end

tester = {'grandpa' => {'dad' => {'child 1' => {}, 'child 2' => {}}, 'uncle' => {'child 3' => {}, 'child 4' => {}}}}
test_shallow = {'grandpa' => {}}
test_deeper = {'grandpa' => {'dad' => {}}}
test_again = {'grandpa' => {}, 'grandma' => {}}

ruby_tree = Tree.new(tester)

puts "Visiting a node"
ruby_tree.visit {|node| puts node.node_name}

puts "Visiting entire tree"
ruby_tree.visit_all {|node| puts node.node_name}


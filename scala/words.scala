trait Censor():
  def worksMap: Map[String, String]
  def swap(word: String): String

class Heya extends Censor:
  val worksMap = Map("shoot" -> "pucky", "darn" -> "beans")
  def swap(someWord: String): String = worksMap.getOrElse(someWord, someWord)
  def censor(userWord: String): Unit = println("The word " + userWord + " is actually " + swap(userWord))


// load ./words.scala
// val x = new Heya()"
// x.censor("shoot") 

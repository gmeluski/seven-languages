object Main {
  def main(args: Array[String]): Unit = {
    println("for loop using ruby-style iteration")
    args.foreach { arg =>
      println(arg)
    }
  }
}

instrument('Trent Rezor', 'vocals').
instrument('Trent Rezor', 'synthesizer').
instrument('Trent Rezor', 'guitar').
instrument('Bad Bunny', 'vocals').
instrument('Joe Talbot', 'vocals').
instrument('Lee Kiernan', 'guitar').
instrument('Jon Beavis', 'drums').

genre('Trent Rezor', 'industrial').
genre('Bad Bunny', 'trap').
genre('Joe Talbot', 'rock').
genre('Lee Kiernan', 'rock').
genre('Jon Beavis', 'rock').
